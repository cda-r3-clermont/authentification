package com.humanbooster.authent.authent_jakarta.models;

import com.humanbooster.authent.authent_jakarta.interfaces.UserValidator;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;

@Entity
@Table(name="utilisateur")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, unique = true)
    @NotBlank(message = "Veuillez saisir un nom d'utilisateur",  groups = UserValidator.class)
    private String username;

    @Column(name = "firstname", nullable = false)
    @NotBlank(message = "Veuillez saisir votre prénom",  groups = UserValidator.class)
    private String firstname;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir votre nom", groups = UserValidator.class)
    private String lastname;

    @Column(nullable = false)
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",
            message = "Veuillez saisir au moins 8 caractères, un nombre et une lettre",
            groups = UserValidator.class)
    private String password;

    @Column(nullable=false)
    private String role;

    public User() {
    }

    public User(String username, String firstname, String lastname, String password, String role) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
