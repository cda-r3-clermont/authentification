package com.humanbooster.authent.authent_jakarta.service;

import com.humanbooster.authent.authent_jakarta.models.User;
import com.humanbooster.authent.authent_jakarta.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class UserService {
    public void addUser(User user){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        session.persist(user);

        tx.commit();
        session.close();
    }

    public List<User> getByUsername(String username){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        List<User> users = session.createQuery("FROM User u WHERE u.username = :username", User.class)
                .setParameter("username", username).getResultList();


        tx.commit();
        session.close();
        
        return users;
    }
}
