package com.humanbooster.authent.authent_jakarta.filters;

import com.humanbooster.authent.authent_jakarta.models.User;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebFilter(filterName = "adminFilter", urlPatterns = "/admin")
public class AdminFilter extends HttpFilter {

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest reqHttp = (HttpServletRequest) req;
        User utilisateur = (User) reqHttp.getSession().getAttribute("user");

        if(utilisateur == null){
            HttpServletResponse resHttp = (HttpServletResponse) res;
            resHttp.sendRedirect("./login");
        }
        if(!utilisateur.getRole().equals("ROLE_ADMIN")){
            HttpServletResponse resHttp = (HttpServletResponse) res;
            resHttp.sendRedirect("./home");
        } else {
            chain.doFilter(req, res);
        }
    }
}
