package com.humanbooster.authent.authent_jakarta.filters;

import com.humanbooster.authent.authent_jakarta.models.User;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebFilter(filterName = "userFilter", urlPatterns = "/home")
public class UserFilter extends HttpFilter {
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest reqHttp = (HttpServletRequest) req;
        User utilisateur = (User) reqHttp.getSession().getAttribute("user");

        if(utilisateur == null){
            HttpServletResponse resHttp = (HttpServletResponse) res;
            resHttp.sendRedirect("./login");
        } else {
            chain.doFilter(req, res);
        }
    }
}
