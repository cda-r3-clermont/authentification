package com.humanbooster.authent.authent_jakarta.servlet;


import com.humanbooster.authent.authent_jakarta.interfaces.UserValidator;
import com.humanbooster.authent.authent_jakarta.models.User;
import com.humanbooster.authent.authent_jakarta.service.UserService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.*;
import org.mindrot.jbcrypt.BCrypt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@WebServlet(name = "register", urlPatterns = "/register")
public class RegisterServlet extends HttpServlet {

    private UserService userService;

    public RegisterServlet() {
        super();
        this.userService = new UserService();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("register.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String lastname = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String password = request.getParameter("password");
        String confirm_password = request.getParameter("confirm_password");

        User utilisateur = new User(username, lastname, prenom, password, "ROLE_USER");

        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();

        Set<ConstraintViolation<User>> errorUsername =
                validator.validateProperty(utilisateur, "username", UserValidator.class);

        List<String> errorUsernameExist = new ArrayList<String>();

        if(userService.getByUsername(username).size() != 0){
            errorUsernameExist.add("Cet utilisateur existe déjà");
        }
        Set<ConstraintViolation<User>> errorFirstname =
                validator.validateProperty(utilisateur, "firstname", UserValidator.class);


        Set<ConstraintViolation<User>> errorLastname =
                validator.validateProperty(utilisateur, "lastname", UserValidator.class);

        Set<ConstraintViolation<User>> errorPassword =
                validator.validateProperty(utilisateur, "password", UserValidator.class);

        List<String> errorConfirm = new ArrayList<>();

        if(!confirm_password.equals(password)){
            errorConfirm.add("Les mots de passes ne correspondent pas");
        }

        if(errorUsernameExist.isEmpty() && errorConfirm.isEmpty() && errorUsername.isEmpty() && errorFirstname.isEmpty() && errorLastname.isEmpty() && errorPassword.isEmpty()){
            utilisateur.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
            this.userService.addUser(utilisateur);
            response.sendRedirect("./login");
        } else {
            request.setAttribute("username",errorUsername);
            request.setAttribute("usernameExist",errorUsernameExist);
            request.setAttribute("firstname", errorFirstname);
            request.setAttribute("lastname", errorLastname);
            request.setAttribute("password", errorPassword);
            request.setAttribute("confirm", errorConfirm);

            request.setAttribute("saisie", utilisateur);

            request.getRequestDispatcher("register.jsp").forward(request, response);
        }

    }
}
