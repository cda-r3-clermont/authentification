package com.humanbooster.authent.authent_jakarta.servlet;

import com.humanbooster.authent.authent_jakarta.models.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "userServlet", urlPatterns = "/home")
public class UserServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User connectedUser = (User) request.getSession().getAttribute("user");
        request.setAttribute("user", connectedUser);

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

}
