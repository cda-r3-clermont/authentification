<%--
  Created by IntelliJ IDEA.
  User: aureliendelorme
  Date: 07/09/2023
  Time: 13:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Bienvenue sur votre application !</title>
</head>
<body>
<h1>Bonjour ${user.firstname} :) !</h1>

<a href="./logout">Me déconnecter</a>
</body>
</html>
