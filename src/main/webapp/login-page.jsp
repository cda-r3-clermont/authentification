<%--
  Created by IntelliJ IDEA.
  User: aureliendelorme
  Date: 05/09/2023
  Time: 11:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Page de connexion !</title>
    <link href="css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="css/login-register.css" rel="stylesheet">
    <script rel="script" src="js/bootstrap.bundle.min.js"></script>

</head>
<body>
<div class="sidenav">
</div>
<div class="main">
    <div class="col-sm-12">
            <h2>Concession <br> Login</h2>
        <div class="login-form">
            <form method="post">
                <div class="form-group">
                    <label>User Name</label>
                    <input type="text" name="username" class="form-control is-valid" placeholder="User Name">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input name="password" type="password" class="form-control is-invalid" placeholder="Password">
                    <c:if test="${error}">
                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                        Invalid Credentials
                    </div>
                    </c:if>
                </div>
                <div class="mt-3">
                    <button type="submit" class="btn btn-black">Login</button>
                    <a href="./register" class="btn btn-secondary">Register</a>
                </div>

            </form>
        </div>
    </div>
</div>

<a class="btn btn-success" href="/authent/register">Je n'ai pas de compte : Me connecter !</a>
</body>
<body>

</body>
</html>
