<%--
  Created by IntelliJ IDEA.
  User: aureliendelorme
  Date: 05/09/2023
  Time: 11:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Création de compte !</title>
    <link href="css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="css/login-register.css" rel="stylesheet">
    <script rel="script" src="js/bootstrap.bundle.min.js"></script>
</head>
<body>
<div class="sidenav">
</div>
<div class="main">
    <!-- CECI est un commentaire HTML -->
    <%--
    Ceci est un commentaire JAVA
    --%>
    <div class="col-sm-12">
        <h2>Concession <br> créer un compte</h2>
        <div class="login-form">
            <form method="post">

                <div class="form-group">
                    <label>Nom d'utilisateur</label>
                    <input name="username" value="${saisie.username}" type="text" class="form-control

                    <c:if test="${username.size() == 0 && usernameExist.size() == 0}">is-valid</c:if>
                     <c:if test="${username.size() != 0 || usernameExist.size() != 0}">is-invalid</c:if>"
                           placeholder="User Name">

                    <c:if test="${username.size() == 0 && usernameExist.size() == 0}">
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </c:if>

                    <c:if test="${username.size() != 0 ||  usernameExist.size() != 0}">
                        <div class="invalid-feedback">
                            <ul>
                                <c:forEach items="${username}" var="error">
                                    <li>${error.message}</li>
                                </c:forEach>

                                <c:forEach items="${usernameExist}" var="errorUsername">
                                    <li>${errorUsername}</li>
                                </c:forEach>
                            </ul>
                        </div>
                    </c:if>
                </div>

                <div class="form-group">
                    <label>Nom de famille</label>
                    <input name="nom" value="${saisie.firstname}" type="text" class="form-control
                     <c:if test="${firstname.size() == 0}">is-valid</c:if>
                     <c:if test="${firstname.size() != 0}">is-invalid</c:if>" placeholder="Nom">


                    <c:if test="${firstname.size() == 0}">
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </c:if>

                    <c:if test="${firstname.size() != 0}">
                        <div class="invalid-feedback">
                            <ul>
                                <c:forEach items="${firstname}" var="error">
                                    <li>${error.message}</li>
                                </c:forEach>
                            </ul>
                        </div>
                    </c:if>

                </div>

                <div class="form-group">
                    <label>Prénom</label>
                    <input type="text" value="${saisie.lastname}" name="prenom" class="form-control
  <c:if test="${lastname.size() == 0}">is-valid</c:if>
                     <c:if test="${lastname.size() != 0}">is-invalid</c:if>" placeholder="Prénom">


                    <c:if test="${lastname.size() == 0}">
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </c:if>

                    <c:if test="${lastname.size() != 0}">
                        <div class="invalid-feedback">
                            <ul>
                                <c:forEach items="${lastname}" var="error">
                                    <li>${error.message}</li>
                                </c:forEach>
                            </ul>
                        </div>
                    </c:if>


                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" value="${saisie.password}" name="password" class="form-control
<c:if test="${password.size() == 0}">is-valid</c:if>
                     <c:if test="${password.size() != 0}">is-invalid</c:if>" placeholder="Password">

                    <c:if test="${password.size() == 0}">
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </c:if>

                    <c:if test="${password.size() != 0}">
                        <div class="invalid-feedback">
                            <ul>
                                <c:forEach items="${password}" var="error">
                                    <li>${error.message}</li>
                                </c:forEach>
                            </ul>
                        </div>
                    </c:if>
                </div>

                <div class="form-group">
                    <label>Confirm password</label>
                    <input type="password" value="" name="confirm_password"
                           class="form-control <c:if test="${confirm.size() == 0}">is-valid</c:if>
                     <c:if test="${confirm.size() != 0}">is-invalid</c:if>" placeholder="User Name">

                    <c:if test="${confirm.size() == 0}">
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </c:if>

                    <c:if test="${confirm.size() != 0}">
                        <div class="invalid-feedback">
                            <ul>
                                <c:forEach items="${confirm}" var="error">
                                    <li>${error}</li>
                                </c:forEach>
                            </ul>
                        </div>
                    </c:if>

                </div>
                <div class="mt-3">
                    <button type="submit" class="btn btn-black">M'enregistrer</button>
                    <a href="./login" class="btn btn-secondary">J'ai déjà un compte</a>
                </div>
            </form>
        </div>
    </div>
</div>

<a class="btn btn-success" href="/jpa-courses/register">Je n'ai pas de compte : Me connecter !</a>
</body>
<body>

</body>
</html>
